
package main

import (
	"fmt"
    
	"github.com/spf13/cobra"

	"mymodule/mypackage"

	"context"

	speech "cloud.google.com/go/speech/apiv1"
)

func main() {

	ctx := context.Background()
	speech.NewClient(ctx)
	cmd := &cobra.Command{
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("Hello, Modules!")

			mypackage.PrintHello()
		},
	}

	fmt.Println("Calling cmd.Execute()!")
	cmd.Execute()

}
